window.addEventListener('load', function () {
	let checkbox = document.getElementById('hamburger-status');
	checkbox.addEventListener('change', function () {
		if (this.checked === true) {
			let hamItems = document.getElementById('hamburger-items');
			let hamImage = document.getElementById('ham-image');
			hamImage.setAttribute('src', 'images/icon-close.svg');
			hamItems.classList.add('disp');
		} else {
			let hamItems = document.getElementById('hamburger-items');
			let hamImage = document.getElementById('ham-image');
			hamImage.setAttribute('src', 'images/icon-hamburger.svg');
			hamItems.classList.remove('disp');
		}
	});
});
