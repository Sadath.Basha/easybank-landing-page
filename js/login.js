window.addEventListener('load', () => {
	let button = document.getElementsByClassName('btn')[0];
	button.disabled = true;
	button.classList.add('btn-disable');
	let email = document.getElementById('email');
	let pass = document.getElementById('pass');
	let pattern = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
	let emailError = document.getElementById('email-error');
	let passError = document.getElementById('pass-error');
	let counter = 0;
	email.addEventListener('input', () => {
		if (pattern.test(email.value)) {
			emailError.innerHTML = '';
			email.classList.remove('invalid');
			counter++;
		} else {
			emailError.innerHTML = 'please enter valid email';
			email.classList.add('invalid');
		}
		if (pattern.test(email.value) && pass.value !== '') {
			button.classList.remove('btn-disable');
		} else {
			button.classList.add('btn-disable');
		}
	});
	pass.addEventListener('input', () => {
		if (pass.value !== '') {
			passError.innerHTML = '';
			pass.classList.remove('invalid');
			counter++;
		} else {
			passError.innerHTML = 'please enter the password';
			pass.classList.add('invalid');
		}
		if (pattern.test(email.value) && pass.value !== '') {
			button.classList.remove('btn-disable');
		} else {
			button.classList.add('btn-disable');
		}
	});
});
