window.addEventListener('load', () => {
	let button = document.getElementsByClassName('btn')[0];
	button.disabled = true;
	button.classList.add('btn-disable');
	let name = document.getElementById('name');
	let email = document.getElementById('email');
	let pass = document.getElementById('pass');
	let confirmPass = document.getElementById('confirm-pass');
	let emailPattern = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
	let namePattern = /^[a-zA-Z ]{2,30}$/;
	let nameError = document.getElementById('name-error');
	let emailError = document.getElementById('email-error');
	let passError = document.getElementById('pass-error');
	let confirmError = document.getElementById('confirm-error');

	name.addEventListener('input', () => {
		let btnStatus =
			namePattern.test(name.value) &&
			emailPattern.test(email.value) &&
			pass.value !== '' &&
			pass.value === confirmPass.value;
		let nameLength = name.value.length;
		if (namePattern.test(name.value)) {
			nameError.innerHTML = '';
			name.classList.remove('invalid');
		} else {
			if (nameLength > 30) {
				nameError.innerHTML = 'name cannot be more than 30 characters';
			} else {
				nameError.innerHTML = 'please enter a valid name more than 3 characters';
			}
			name.classList.add('invalid');
		}

		if (btnStatus) {
			button.classList.remove('btn-disable');
		} else {
			button.classList.add('btn-disable');
		}
	});

	email.addEventListener('input', () => {
		let btnStatus =
			namePattern.test(name.value) &&
			emailPattern.test(email.value) &&
			pass.value !== '' &&
			pass.value === confirmPass.value;
		if (emailPattern.test(email.value)) {
			emailError.innerHTML = '';
			email.classList.remove('invalid');
		} else {
			emailError.innerHTML = 'please enter valid email';
			email.classList.add('invalid');
		}

		if (btnStatus) {
			button.classList.remove('btn-disable');
		} else {
			button.classList.add('btn-disable');
		}
	});
	pass.addEventListener('input', () => {
		let btnStatus =
			namePattern.test(name.value) &&
			emailPattern.test(email.value) &&
			pass.value !== '' &&
			pass.value === confirmPass.value;
		if (pass.value !== '') {
			passError.innerHTML = '';
			pass.classList.remove('invalid');
		} else {
			passError.innerHTML = 'please enter the password';
			pass.classList.add('invalid');
		}
		if (emailPattern.test(email.value) && pass.value !== '') {
			button.classList.remove('btn-disable');
		} else {
			button.classList.add('btn-disable');
		}
	});

	confirmPass.addEventListener('input', () => {
		let btnStatus =
			namePattern.test(name.value) &&
			emailPattern.test(email.value) &&
			pass.value !== '' &&
			pass.value === confirmPass.value;
		if (pass.value === confirmPass.value) {
			confirmError.innerHTML = '';
			confirmPass.classList.remove('invalid');
		} else {
			confirmError.innerHTML = 'password not matching';
			confirmPass.classList.add('invalid');
		}

		if (btnStatus) {
			button.classList.remove('btn-disable');
		} else {
			button.classList.add('btn-disable');
		}
	});
});
